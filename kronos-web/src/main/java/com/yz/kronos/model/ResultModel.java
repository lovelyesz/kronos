package com.yz.kronos.model;

import lombok.Data;

/**
 * @author shanchong
 * @date 2021/2/19
 **/
@Data
public class ResultModel<T> {

    /**
     * 编号
     */
    private int code;

    private String msg;

    private T data;

    private final static int SUCCESS_CODE = 200;

    private final static String SUCCESS_MSG = "成功";

    private final static int ERROR_CODE = -1;

    public static ResultModel success() {
        ResultModel resultModel = new ResultModel<>();
        resultModel.setCode(SUCCESS_CODE);
        resultModel.setMsg(SUCCESS_MSG);
        return resultModel;
    }

    public static <T> ResultModel<T> success(T data) {
        ResultModel<T> resultModel = success();
        resultModel.setData(data);
        return resultModel;
    }

    public static ResultModel<Object> error(String msg) {
        ResultModel<Object> resultModel = new ResultModel<>();
        resultModel.setCode(ERROR_CODE);
        resultModel.setMsg(msg);
        return resultModel;
    }

}
