package com.yz.kronos.common;

import lombok.Data;

/**
 * @author shanchong
 * @date 2021/11/23
 **/
@Data
public class TriggerData {

    /**
     * 触发类的全名
     */
    private String clazz;
    /**
     * 方法
     */
    private String method;
    /**
     * 分片总数
     */
    private Integer shareTotal;
    /**
     * 分片索引
     */
    private Integer index;
    /**
     * 扩展信息
     */
    private String extend;

}
