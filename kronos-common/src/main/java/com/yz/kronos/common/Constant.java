package com.yz.kronos.common;

/**
 * 常量
 * @author shanchong
 * @date 2021/2/10
 **/
public class Constant {

    /**
     * 当前分片数量
     */
    public static final String SHARE_TOTAL = "KRONOS_SHARE_TOTAL";

    /**
     * 当前分片索引
     */
    public static final String SHARE_INDEX = "KRONOS_INDEX";

    /**
     * 执行器名称
     */
    public static final String EXECUTOR = "KRONOS_EXECUTOR";

    /**
     * 触发器参数
     */
    public static final String TRIGGER_PARAMS = "kronos.trigger.param";

    /**
     * 调度器地址
     */
    public static final String SCHEDULER_URL = "scheduler.url";
    /**
     * 模式
     */
    public static final String MODEL = "model";
}
