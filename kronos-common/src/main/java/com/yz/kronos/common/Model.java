package com.yz.kronos.common;

/**
 * @author shanchong
 */

public enum Model {
    /**
     * 单机/分布式
     */
    STAND_ALONE,
    DISTRIBUTED;

}
