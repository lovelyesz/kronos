package com.yz.kronos.schedule.api;

import cn.hutool.json.JSONUtil;
import com.yz.kronos.schedule.handle.ScheduleHandler;
import com.yz.kronos.schedule.model.KronosConfig;
import com.yz.kronos.schedule.model.RequestDto;
import io.fabric8.kubernetes.api.model.batch.Job;
import io.fabric8.kubernetes.api.model.batch.JobList;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.ConfigBuilder;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ScheduleApiTest {

    @Autowired
    private ScheduleHandler scheduleHandler;
    @Autowired
    private KronosConfig kronosConfig;

    @Test
    public void test() {
        List<RequestDto> objects = new ArrayList<>();
        RequestDto requestDto = new RequestDto();
        requestDto.setCmd("./demo-kronos/start.sh");
        requestDto.setImage("registry.cn-qingdao.aliyuncs.com/kronos/kronos-demo:0.0.14");
        requestDto.setResources("{'cpu':1,'memory':'1024Mi'}");
        requestDto.setExecutor("com.yz.kronos.demo.service.DemoService#test");
        requestDto.setShareTotal(5);
        objects.add(requestDto);
        System.out.println(JSONUtil.toJsonStr(objects));
        scheduleHandler.handle(objects);
    }

    @Test
    public void test1() {
        Config config = new ConfigBuilder()
                .withMasterUrl(kronosConfig.getServiceApi())
                .withTrustCerts(true)
                .build();
        DefaultKubernetesClient defaultKubernetesClient = new DefaultKubernetesClient(config);
        JobList jobList = defaultKubernetesClient.inNamespace("kronos").batch().jobs().list();
        for (Job job : jobList.getItems()) {
            defaultKubernetesClient.inNamespace("kronos").batch().jobs().create(job);
            break;
        }
    }

}
