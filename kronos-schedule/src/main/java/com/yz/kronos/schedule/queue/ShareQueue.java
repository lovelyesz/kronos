package com.yz.kronos.schedule.queue;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author shanchong
 * @date 2021/2/9
 **/
public class ShareQueue {

    static Map<String, AtomicInteger> queueMap = new HashMap<>();

    public static void put(String flowId, int shareTotal) {
        AtomicInteger atomicInteger = queueMap.getOrDefault(flowId, new AtomicInteger(shareTotal));
        queueMap.put(flowId, atomicInteger);
    }

    public synchronized static Integer pop(String flowId) {
        AtomicInteger executionParam = queueMap.get(flowId);
        if (executionParam == null) {
            return 0;
        }
        int i = executionParam.decrementAndGet();
        return Math.max(i, 0);
    }
}
