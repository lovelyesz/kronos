package com.yz.kronos.schedule.api;

import com.yz.kronos.common.TriggerData;
import com.yz.kronos.schedule.handle.ScheduleHandler;
import com.yz.kronos.schedule.model.RequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 任务调度
 * @author shanchong
 * @date 2021/2/6
 **/
@RestController
@RequestMapping(value = "/schedule")
public class ScheduleApi {

    @Autowired
    private ScheduleHandler scheduleHandler;

    @PostMapping(value = "/handle")
    public String handle(@RequestBody List<RequestDto> requestDtoList) {
        scheduleHandler.handle(requestDtoList);
        return "OK";
    }

    @PostMapping(value = "/interrupt")
    public boolean interrupt(String id) {
        return scheduleHandler.interrupt(id);
    }

    @GetMapping(value = "/pulljob")
    public TriggerData pullJob() {
        TriggerData triggerData = new TriggerData();
        triggerData.setClazz("demoService");
        triggerData.setMethod("test");
        triggerData.setShareTotal(2);
        triggerData.setIndex(1);
        triggerData.setExtend("67272727");
        return triggerData;
    }
}
