package com.yz.kronos.schedule.model;

import lombok.Data;

/**
 * @author shanchong
 * @date 2021/2/6
 **/
@Data
public class RequestDto {

    private String executor;
    /**
     * 分片数量
     */
    private int shareTotal;
    /**
     * 镜像地址docker
     */
    private String image;
    /**
     * 执行命令
     */
    private String cmd;
    /**
     * 运行资源
     */
    private String resources;
    /**
     * 命名空间
     */
    private String namespace;
}
