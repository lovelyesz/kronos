package com.yz.kronos.schedule.api;

import com.yz.kronos.schedule.queue.ShareQueue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author shanchong
 * @date 2021/2/9
 **/
@RestController
public class QueueApi {

    @GetMapping(value = "/queue/index")
    public Integer getIndex(@RequestParam String flowId) {
        return ShareQueue.pop(flowId);
    }

}
