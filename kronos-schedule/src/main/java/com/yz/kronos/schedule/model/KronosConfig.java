package com.yz.kronos.schedule.model;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Set;

/**
 * @author shanchong
 * @date 2019-11-12
 **/
@Data
@ConfigurationProperties(prefix = "kronos")
public class KronosConfig {

    /**
     * 重试策略
     */
    private String restartPolicy;

    /**
     * kubernetes的地址
     */
    private String serviceApi;

    /**
     * 镜像拉取策略 IfNotPresent,Always
     */
    private String imagePullPolicy;

    /**
     * 日志输出路径
     */
    private String logPath;

    private String apiVersion;

    private String groupName;
    /**
     * 刷新任务状态的频率
     */
    private Long resyncPeriodInMillis;
    /**
     * 脚本中回调获取执行器索引地址
     */
    private String schedulerUrl;
    /**
     * 命名空间
     */
    private Set<String> namespaces;

}
