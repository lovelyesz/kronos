# 介绍
    
Kronos是基于kubernetes的高性能的流式分布式任务调度系统，在日常开发工作中我们往往避免不了要处理一些比较复杂的流式任务，市面上的定时任务系统的实现方式也是多种多样，优缺点各有不同，在此之前最常见的ElasticJob和XxlJob。他们的实现原理这里就不再缀诉了，有兴趣的小伙伴可以去了解一下。

任务之间经常存在着先依赖关系，比如B任务必须在A任务执行完成后再执行，传统的解决办法就是在两个任务触发时间之间预留出足够长的时间，不仅会浪费一些时间，执行结果也不稳定，依赖关系不方便维护

分片是指将比较大的任务拆分成若干分，分散到不同的进程中运行，每一片都可以拥有足够的物理资源，避免线程间的资源竞争

kronos使用Kubernetes实现资源的合理调度

# 使用Kubernetes

k8s在生成Job时声明Job所需要的资源（CPU、内存、带宽等）以及任务要分片的数量，每一片任务都是一个单独的Pod并独享资源，这样就能够避免了任务间的资源竞争问题，Job组件在执行完所有的Pod后会自动释放资源，当资源不足的时候会停止生成Pod等待资源。

# Kronos设计方案

![输入图片说明](https://images.gitee.com/uploads/images/2020/0620/154650_ac66e0e6_1074282.jpeg "Untitled Diagram.jpg")

Kronos项目分成两部分：kronos-schedule、kronos-executor
kronos-schedule：负责任务的触发、调度、声明、以及k8s资源的监控，对外提供api
kronos-executor：任务的执行器，业务容器项目引入executor依赖后打包docker镜像上传到仓库中，k8s在生成Pod时会pull下来这个镜像
![输入图片说明](https://images.gitee.com/uploads/images/2021/0210/214433_99f5be2e_1074282.png "kronos (1).png")
# 搭建开发环境
[minikube方式](https://gitee.com/lovelyesz/kronos/blob/master/ENVIRONMENT.md)
[Docker Desktop方式](https://gitee.com/lovelyesz/kronos/blob/master/DOCKERDESKTOP.md)
