package com.yz.kronos.executor.trigger;

/**
 * 触发器
 * @author shanchong
 */
public interface Trigger {

    /**
     * 获取触发参数
     * @return 触发参数
     */
    String getTriggerParams();

}
