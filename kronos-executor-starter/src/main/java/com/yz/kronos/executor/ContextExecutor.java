package com.yz.kronos.executor;

import com.yz.kronos.executor.context.ExecutorContext;

/**
 * 获取上下文的执行器
 * @author shanchong
 * @date 2021/11/22
 **/
public interface ContextExecutor {

    /**
     * 设置上下文
     * @param context 执行器上下文
     */
    void setContext(ExecutorContext context);

}
