package com.yz.kronos.executor.trigger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;

import static com.yz.kronos.common.Constant.TRIGGER_PARAMS;

/**
 * 单机触发器
 * @author shanchong
 * @date 2021/11/22
 **/
public class StandAloneTrigger extends AbstractTrigger {

    @Autowired
    ApplicationArguments applicationArguments;
    /**
     * 获取触发参数
     *
     * @return 触发参数
     */
    @Override
    public String getTriggerParams() {
        return applicationArguments.getOptionValues(TRIGGER_PARAMS).get(0);
    }

}
