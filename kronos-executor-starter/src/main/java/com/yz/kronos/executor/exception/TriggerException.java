package com.yz.kronos.executor.exception;

/**
 * 触发器异常
 * @author shanchong
 * @date 2021/11/22
 **/
public class TriggerException extends RuntimeException {

    public TriggerException(final String s) {
        super(s);
    }
}
