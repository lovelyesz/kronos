package com.yz.kronos.executor.exception;

/**
 * 执行器异常
 * @author shanchong
 * @date 2021/11/22
 **/
public class ExecutorException extends RuntimeException {

    public ExecutorException(final String message) {
        super(message);
    }
}
