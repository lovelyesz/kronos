package com.yz.kronos.executor.context;

/**
 * 执行上下文
 * @author shanchong
 */
public class ExecutorContext {

    /**
     * 分片总数量
     */
    private Integer shareTotal;
    /**
     * 当前分片的索引
     */
    private Integer index;
    /**
     * 扩展参数
     */
    private String extend;

    public ExecutorContext(final Integer shareTotal, final Integer index) {
        this.shareTotal = shareTotal;
        this.index = index;
    }

    public ExecutorContext(final Integer shareTotal, final Integer index, final String extend) {
        this.shareTotal = shareTotal;
        this.index = index;
        this.extend = extend;
    }

    public Integer getShareTotal() {
        return shareTotal;
    }

    public void setShareTotal(final Integer shareTotal) {
        this.shareTotal = shareTotal;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(final Integer index) {
        this.index = index;
    }

    public String getExtend() {
        return extend;
    }

    public void setExtend(final String extend) {
        this.extend = extend;
    }
}
