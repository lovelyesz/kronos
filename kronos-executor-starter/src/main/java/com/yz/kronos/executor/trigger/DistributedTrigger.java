package com.yz.kronos.executor.trigger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;

import java.util.List;

import static com.yz.kronos.common.Constant.SCHEDULER_URL;

/**
 * 分布式触发器
 * @author shanchong
 * @date 2021/11/23
 **/
public class DistributedTrigger extends AbstractTrigger {

    @Autowired
    private ApplicationArguments applicationArguments;
    /**
     * 获取触发参数
     * @return 触发参数
     */
    @Override
    public String getTriggerParams() {
        List<String> schedulerUrls = applicationArguments.getOptionValues(SCHEDULER_URL);
        String schedulerUrl = schedulerUrls.get(0);
        

        return null;
    }

}
