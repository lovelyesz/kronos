
## 介绍

minikube是一个简化版的kubernetes，用于实验教学，运行需要一个虚拟环境，一般使用VB或docker，启动的时候使用参数声明

`minikube start --driver=docker`
或者
`minikube start --driver=virtualbox`
## 下载
  这里使用的是ali的镜像
`curl -Lo minikube http://kubernetes.oss-cn-hangzhou.aliyuncs.com/minikube/releases/v1.16.0/minikube-darwin-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/`
## 创建
`minikube start --registry-mirror=https://registry.docker-cn.com`

## 获取master-api地址
`kubectl cluster-info`

## 开启控制台
`minikube dashboard`

## 匿名用户授权
`kubectl create clusterrolebinding test:anonymous --clusterrole=cluster-admin --user=system:anonymous`

## 创建命名空间
`kubectl create namespace kronos`

## 参考
[参考文档](https://kubernetes.io/docs/tutorials/hello-minikube/)

