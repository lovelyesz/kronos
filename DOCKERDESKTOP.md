## 下载安装
`https://desktop.docker.com/mac/stable/Docker.dmg`
## 安装镜像
`git clone https://github.com/maguowei/k8s-docker-desktop-for-mac.git`
`cd k8s-docker-desktop-for-mac`
`./load_images.sh`
## 在Docker for Mac 设置中启用 Kubernetes 选项, 并等待一会儿，直到 Kubernetes 开始运行。
https://github.com/maguowei/k8s-docker-desktop-for-mac/blob/master/image/k8s.png
## 开启代理
`kubectl proxy`
